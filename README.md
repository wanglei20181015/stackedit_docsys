# StackEdit-docsys

### 项目启动

下载项目名固定stackedit

 ![image-20210708231104495](img/README/image-20210708231104495.png)

附带资料,node和python2.7

链接：https://pan.baidu.com/s/1H9INTpaFKPeH9wUnllyD6Q 
提取码：1234

安装node,无脑安装就行，最后一步auto啥的选上，会自动安装一个编译环境，看弹窗提示

安装python2.7,无脑装即可

安装vpn软件，亲测免费且好用，佛跳墙

https://d2fjjqle8ae8y0.cloudfront.net/

### 项目编译步骤

```bash
# 1.通过cnpm使用淘宝镜像：
npm install -g cnpm --registry=https://registry.npm.taobao.org

# 2.修改设置，解除ssl验证
git config --global http.sslVerify "false"

# 3.安装gulp环境
npm install --global gulp

# 4.install dependencies，注意其中有些报错是正常现象
cnpm install

# 5.serve with hot reload at localhost:8080
npm start
```

### 项目编译

```bash
# 编译执行如下指令
npm run build
```

### 错误问题解决

prismjs报错,该问题是cnpm安装依赖导致，自动升级包了

1.直接手动删除node_nodules包下的prismjs包

 ![image-20210708232033246](img/README/image-20210708232033246.png)

2.执行下载项目对应版本

```bash
cnpm install prismjs@1.6.0
```

 ![image-20210708232157068](img/README/image-20210708232157068.png)

