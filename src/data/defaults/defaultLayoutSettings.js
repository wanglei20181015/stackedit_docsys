export default () => ({
  showNavigationBar: true,
  showEditor: false,
  showSidePreview: true,
  showStatusBar: true,
  showSideBar: false,
  showExplorer: false,
  scrollSync: true,
  focusMode: false,
  findCaseSensitive: false,
  findUseRegexp: false,
  sideBarPanel: 'menu',
  welcomeTourFinished: false,
});
